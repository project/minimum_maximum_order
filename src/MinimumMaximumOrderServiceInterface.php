<?php

namespace Drupal\minimum_maximum_order;

use Drupal\commerce_order\Entity\Order;

/**
 * Interface MinimumMaximumOrderServiceInterface.
 *
 * @package Drupal\minimum_maximum_order
 */
interface MinimumMaximumOrderServiceInterface {

  /**
   * Returns whether this module features are enabled
   *
   * @return bool
   */
  public function isEnabled();

  /**
   * Determine if order meets minimum and maximum
   *
   * @param Order $order
   * @return bool
   */
  public function meetsMinimumMaximumRequirement(Order $order);

  /**
   * Determine if order meets minimum requirement
   *
   * @param Order $order
   * @return bool
   */
  public function meetsMinimumRequirement(Order $order);

  /**
   * Determine if order meets maximum requirement
   *
   * @param Order $order
   * @return bool
   */
  public function meetsMaximumRequirement(Order $order);

  /**
   * Determine if any of the roles passed are allowed to bypass
   * min and max requirements
   *
   * @param string[] $roles
   * @return bool
   */
  public function canRoleBypass($roles);

  /**
   * Undocumented function
   *
   * @return string
   */
  public function getMinimumWarningMessage();

  /**
   * Undocumented function
   *
   * @return string
   */
  public function getMaximumWarningMessage();
}
