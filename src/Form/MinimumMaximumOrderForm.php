<?php
/**
 * @file
 * Contains Drupal\minimum_maximum_order\Form\MinimumMaximumOrderForm.
 */
namespace Drupal\minimum_maximum_order\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

class MinimumMaximumOrderForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'minimum_maximum_order.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'minimum_maximum_order_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('minimum_maximum_order.adminsettings');

    $form['minimum_order'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum Order'),
      '#description' => $this->t('Minimum order amount needed to go to checkout page'),
      '#default_value' => $config->get('minimum_order')
    ];
    $form['min_custom_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum Order Not Met Message'),
      '#description' => $this->t('The error message displayed when user hasn\'t met the minimum order'),
      '#default_value' => $config->get('min_custom_message')
    ];

    $form['maximum_order'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum Order'),
      '#description' => $this->t('Maximum order amount needed to go to checkout page'),
      '#default_value' => $config->get('maximum_order'),
    ];
    $form['max_custom_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum Order Exceeded Message'),
      '#description' => $this->t('The error message displayed when user has exceeded the maximum order amount'),
      '#default_value' => $config->get('max_custom_message')
    ];

    $form['transition'] = [
      '#type' => 'radios',
      '#title' => $this->t('Transition'),
      '#description' => $this->t('When to check if order meets conditions'),
      '#default_value' => $config->get('transition'),
      '#options' => [
        $this->t('Disable'),
        $this->t('Subtotal(Menu price)'),
        // $this->t('After promotions and adjuments are applied'),
        $this->t('Total(After tax and fees applied)')
      ]
    ];

    $roles = Role::loadMultiple();
    $role_options = [];
    array_map(function($key, $value) use (&$role_options) {
      $role_options[$key] = $this->t($value->get('label'));
    }, array_keys($roles), $roles);
    $form['role_bypass'] = [
      '#title' => $this->t('Bypass by role'),
      '#description' => $this->t('Allow the chosen roles to bypass working hours and order regardless of the chosen hours'),
      '#type' => 'checkboxes',
      '#options' => $role_options,
      '#default_value' => $config->get('role_bypass')
    ];

    $form['enable'] = [
      '#title' => $this->t('Enable'),
      '#type' => 'checkbox',
      '#description' => $this->t('Check this box to enable this module.'),
      '#default_value' => $config->get('enable')
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $min = $form_state->getValue('minimum_order');
    $min_custom_message = $form_state->getValue('min_custom_message');
    $max = $form_state->getValue('maximum_order');
    $max_custom_message = $form_state->getValue('max_custom_message');
    if (!empty($min)) {
      if ($min < 0) {
        $form_state->setError($form['minimum_order'],'Value for minimum order must be greater than 0');
      }
    }
    if (!empty($max)) {
      if ($form_state->getValue('maximum_order') < 0) {
        $form_state->setError($form['maximum_order'],'Value for maximum order must be greater than 0');
      }
    }
    if (!empty($min) && !empty($max) && $min >= $max) {
      $form_state->setError($form['minimum_order'],'Value for minimum order must be less than the maximum order');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    $this->config('minimum_maximum_order.adminsettings')
      ->set('minimum_order', $form_state->getValue('minimum_order'))
      ->set('min_custom_message', $form_state->getValue('min_custom_message'))
      ->set('maximum_order', $form_state->getValue('maximum_order'))
      ->set('max_custom_message', $form_state->getValue('max_custom_message'))
      ->set('transition', $form_state->getValue('transition'))
      ->set('enable', $form_state->getValue('enable'))
      ->set('role_bypass', $form_state->getValue('role_bypass'))
      ->save();
  }
}
