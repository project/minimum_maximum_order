<?php

namespace Drupal\minimum_maximum_order;

use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class MinimumMaximumOrderService.
 *
 * @package Drupal\minimum_maximum_order
 */
class MinimumMaximumOrderService implements MinimumMaximumOrderServiceInterface {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('minimum_maximum_order.adminsettings');
  }

  private function getTotal(Order $order) {
    $config = $this->config->get();
    $when = $config['transition'];
    switch($when) {
      // Disabled
      case 0:
        return NULL;
      // Menu Price
      case 1:
        if (!$order->getSubtotalPrice()) {
          return NULL;
        }
        return $order->getSubtotalPrice()->getNumber();
      break;
      // After tax applied
      case 2:
        if (!$order->getTotalPrice()) {
          return NULL;
        }
        return $order->getTotalprice()->getNumber();
      break;
      default:
        return NULL;
    }
  }

  public function isEnabled() {
    $config = $this->config->get();
    return isset($config['enable']) ? $config['enable'] : TRUE;
  }

  public function meetsMinimumMaximumRequirement(Order $order) {
    $config = $this->config->get();

    $when = $config['transition'];
    $minimum = (int) $config['minimum_order'];
    $maximum = (int) $config['maximum_order'];

    $total_price = $this->getTotal($order);
    if ($total_price > $maximum || $total_price < $minimum) {
      return FALSE;
    }
    return TRUE;
  }


  public function meetsMinimumRequirement(Order $order) {
    $config = $this->config->get();
    $minimum = (int) $config['minimum_order'];

    $total_price = $this->getTotal($order);
    if ($minimum && $total_price < $minimum) {
      return FALSE;
    }
    return TRUE;
  }

  public function meetsMaximumRequirement(Order $order) {
    $config = $this->config->get();
    $maximum = (int) $config['maximum_order'];

    $total_price = $this->getTotal($order);
    if ($maximum && $total_price > $maximum) {
      return FALSE;
    }
    return TRUE;
  }

  public function getMinimumWarningMessage() {
    $config = $this->config->get();
    $min_custom_message = $config['min_custom_message'];
    $minimum = (int) $config['minimum_order'];

    if (!$min_custom_message) {
      return $minimum . ' minimum not met';
    } else {
      return $min_custom_message;
    }
  }

  public function getMaximumWarningMessage() {
    $config = $this->config->get();
    $max_custom_message = $config['max_custom_message'];
    $maximum = (int) $config['maximum_order'];

    if (!$max_custom_message) {
      return $maximum . ' maximum exceeded';
    } else {
      return $max_custom_message;
    }
  }

  public function canRoleBypass($roles) {
    $config = $this->config->get();
    if (!isset($config['role_bypass'])) {
      return FALSE;
    }
    $bypass = array_filter($config['role_bypass'], function($opt) {
      return $opt !== 0;
    });
    return empty(!array_intersect($roles, $bypass));
  }

}
